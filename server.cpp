#include "lib/crow_all.h"
using namespace std;

int main()
{
  crow::SimpleApp app;

  CROW_ROUTE(app, "/")
  .methods("POST"_method)
  ([](const crow::request& req){
    return "Hello, world!";
  });

  CROW_ROUTE(app, "/add_transaction")
  .methods("POST"_method)
  ([](const crow::request& req){
    auto x = crow::json::load(req.body);

    if (!x)
        return crow::response(400);
    
    int amount = x["amount"].i();
    string from = x["from"].s();
    string to = x["to"].s();

    std::ostringstream os;
    
    cout << "New transaction: " << endl;
    cout << "\tfrom \t: "   << from << endl;
    cout << "\tto \t: "     << to   << endl;
    cout << "\tamount \t: " << amount << endl;

    os << "from \t: "   << from << endl;
    os << "to \t: "     << to   << endl;
    os << "amount \t: " << amount << endl;
    
    return crow::response{os.str()};
  });

  app
    .port(8080)
    .run();
}