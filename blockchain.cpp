#include "lib/crow_all.h"
#include <iostream>
#include <string>
#include <sstream>
#include <functional>
#include <algorithm>
#include <vector>
#include <time.h>
using namespace std;


class Transaction
{
private:
	string sender;
	string receiver;
	string amount;

public:
	Transaction();

	void setSender( string );
	void setReceiver( string );
	void setAmount( string);

	string getSender();
	string getReceiver();
	string getAmount();
};

Transaction::Transaction() {}
void Transaction::setSender(string sender) {
	this->sender = sender;
}
void Transaction::setReceiver(string receiver) {
	this->receiver = receiver;
}
void Transaction::setAmount(string amount) {
	this->amount = amount;
}
string Transaction::getSender() {
	return this->sender;
}
string Transaction::getReceiver() {
	return this->receiver;
}
string Transaction::getAmount() {
	return this->amount;
}

class Block
{
	private :
		int index;
		time_t time;
		vector<Transaction> data;
		size_t prev_hash;
		size_t hash_block;

	public :
		Block();
		size_t hashBlock() ;

		void setIndex(int index);
		void setTime(time_t time);
		void setData(vector<Transaction> data);
		void setPrev_hash(size_t prev_hash);
		void setHash_block(size_t hash_block);

		int getIndex();
		time_t getTime();
		vector<Transaction> getData();
		size_t getPrev_hash();
		size_t getHash_block();

		void printData();
};

Block::Block() {}
void Block::setIndex(int index) {
	this->index = index;
}
void Block::setTime(time_t time) {
	this->time = time;
}
void Block::setData(vector<Transaction> data) {
	this->data = data;
}
void Block::setPrev_hash(size_t prev_hash) {
	this->prev_hash = prev_hash;
}
void Block::setHash_block(size_t hash_block) {
	this->hash_block = hash_block;
}

int Block::getIndex() {
	return this->index;
}
time_t Block::getTime() {
	return this->time;
}
vector<Transaction> Block::getData() {
	return this->data;
}
size_t Block::getPrev_hash() {
	return this->prev_hash;
}
size_t Block::getHash_block() {
	return this->hash_block;
}
size_t Block::hashBlock() {
	hash<string> hash_fn;
	return hash_fn(to_string(index) + to_string(time) + to_string(prev_hash) + to_string(hash_block));
}
void Block::printData() {
	for (int i = 0; i < this->data.size(); ++i)
	{
		cout << "====================================" << endl;
		cout << "\t\t_ Send by: \t" << this->data.at(i).getSender() << endl;
		cout << "\t\t_ Receive by: \t" << this->data.at(i).getReceiver() << endl;
		cout << "\t\t_ Amount: \t" << this->data.at(i).getAmount() << endl;
	}
}

class Blockchain
{
private:
	vector<Block> chain;
	vector<Transaction> currentTransaction;

public:
	Blockchain();

	void addBlock(Block);
	void mineCurrent();
	void addTransaction(Transaction);
	std::vector<Block> getChain();
	Block getLastBlock();
};

Blockchain::Blockchain() {}
void Blockchain::addBlock(Block block) {
	this->chain.push_back(block);
}
void Blockchain::mineCurrent() {
	std::stringstream ss;

	Block block;
	Block lastBlock = this->getLastBlock();

	block.setIndex(lastBlock.getIndex() + 1);
	block.setTime(time(0));
	block.setData(this->currentTransaction);
	block.setPrev_hash(lastBlock.getHash_block());
	block.setHash_block(block.hashBlock());

	this->currentTransaction.empty();

	this->chain.push_back(block);
}
void Blockchain::addTransaction(Transaction transaction) {
	this->currentTransaction.push_back(transaction);
}
vector<Block> Blockchain::getChain() {
	return this->chain;
}
Block Blockchain::getLastBlock() {
	return this->chain.back();
}

Block 	createGenesis();
Block 	createNextBlock(Block*);
void 	displayBlock(Block*);

int main(int argc, char const *argv[])
{
	crow::SimpleApp app;
	Blockchain blockchain;
	blockchain.addBlock(createGenesis());

	cout << "-= Hello in my cpp BlockChain =-" << endl;
	cout << endl << endl;
	displayBlock(&blockchain.getLastBlock());

	// for (int i = 0; i < 5; ++i)
	// {
	// 	for (int j = 0; j < i; ++j)
	// 	{
	// 		stringstream ss;
	// 		Transaction transaction;
	// 		transaction.setSender("abc");
	// 		ss << "def " << (j * i) + j;
	// 		transaction.setReceiver(ss.str());
	// 		transaction.setAmount("12");

	// 		blockchain.addTransaction(transaction);
	// 	}
	// 	blockchain.mineCurrent();
	// 	displayBlock(&blockchain.getLastBlock());
	// }

	CROW_ROUTE(app, "/add_transaction")
		.methods("POST"_method)
		([](const crow::request& req){
		auto x = crow::json::load(req.body);

		if (!x)
		    return crow::response(400);

		int amount = x["amount"].i();
		string from = x["from"].s();
		string to = x["to"].s();

		std::ostringstream os;

		cout << "New transaction: " << endl;
		cout << "\tfrom \t: "   << from << endl;
		cout << "\tto \t: "     << to   << endl;
		cout << "\tamount \t: " << amount << endl;

		Transaction transaction;
		transaction.setSender(from);
		transaction.setReceiver(to);
		transaction.setAmount(amount);

		blockchain.addTransaction(transaction);

		return crow::response{os.str()};
	});

		 	

	app
	.port(8080)
	.run();

	return 0;
}

Block createGenesis() {
	Block block;

	Transaction transaction;
	transaction.setSender("abc");
	transaction.setReceiver("def");
	transaction.setAmount("12");

	vector<Transaction> datas;
	datas.push_back(transaction);

	block.setIndex(0);
	block.setTime(time(0));
	block.setData(datas);
	block.setPrev_hash(0);
	block.setHash_block(block.hashBlock());

	return block;
}

void displayBlock(Block *block) {
	time_t date = block->getTime();
	char *timeStamp = asctime(gmtime(&date));
	string timer = string(timeStamp);

 	timer.erase(remove(timer.begin(), timer.end(), '\n'), timer.end());

	cout << "\t-= Block id : " << block->getIndex() << " =-" << endl;
	cout << "\t-= Block time : " << timer << " =-" << endl;
	cout << "\t-= Block data : " << endl;
	block->printData();
	cout << "\t-= Block prev_hash : " << block->getPrev_hash() << " =-" << endl;
	cout << "\t-= Block hash_block : " << block->getHash_block() << " =-" << endl;

	cout << endl << endl;
}